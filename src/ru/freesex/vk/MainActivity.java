package ru.freesex.vk;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.*;

public class MainActivity extends Activity
{
	

	private static WebView mWebView;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    //Инициализация вьюшки
		mWebView = (WebView) findViewById(R.id.mainWebView1);

		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.loadUrl("http://m.vk.com");
		mWebView.setWebViewClient(new HelloWebViewClient());
		}
		
	
	//Инициализация клиента
	private class HelloWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)     {
			view.loadUrl(url);
			return true;
		}
		
		
		
	}
	public void notify(View view){
		mWebView.loadUrl("http://m.vk.com/feed?section=notifications");
	}
	public void me(View view){
		mWebView.loadUrl("http://m.vk.com/id0");
	}
	public void docs(View view){
		mWebView.loadUrl("http://m.vk.com/");
	}
	public void music(View view){
		mWebView.loadUrl("http://m.vk.com/audio");
	}
	public void groups(View vew){
		mWebView.loadUrl("http://m.vk.com/groups");
	}
	public void friends(View view){
		mWebView.loadUrl("http://m.vk.com/friends");
	}
	public void likes(View view){
		mWebView.loadUrl("http://m.vk.com/fave");
	}
	public void photo(View view){
		mWebView.loadUrl("http://m.vk.com/http://m.vk.com/feed?section=photos");
	}
	public void mess(View view){
		mWebView.loadUrl("http://m.vk.com/im");
	}
}